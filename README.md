# To install or update: 
[For Windows](https://cdn.discordapp.com/attachments/745026624260407346/840890817124237352/update.bat)

[For MacOS/Linux](https://cdn.discordapp.com/attachments/745026624260407346/841374639967305808/update.sh)

Install minecraft 1.12.2 and forge 1.12.2 and put the corresponding file (see above) in your .minecraft directory and run it to install or update

![trole](https://cdn.discordapp.com/emojis/826161782241755188.png?v=1)
